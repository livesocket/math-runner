FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/math-runner
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/math-runner
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o math-runner

FROM scratch as release
COPY --from=builder /repos/math-runner/math-runner /math-runner
EXPOSE 8080
ENTRYPOINT ["/math-runner"]
