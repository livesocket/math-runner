package main

import (
	"log"
	"os"
	"os/signal"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/math-runner/runner"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService(nil, []lib.Subscription{runner.OnMessageSubscription}, "")
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}

func registerCommand(name string, proc string, restricted bool) {
	channels, err := getChannels()
	if err != nil {
		panic(err)
	}

	for _, channel := range channels {
		_, err = service.Socket.SimpleCall("private.command.register", nil, wamp.Dict{
			"proc":       proc,
			"channel":    channel,
			"name":       name,
			"activated":  true,
			"restricted": restricted,
		})
		if err != nil {
			panic(err)
		}
	}
}

func getChannels() ([]string, error) {
	result, err := service.Socket.SimpleCall("private.channel.get", nil, nil)
	if err != nil {
		return nil, err
	}
	results := []string{}
	for _, item := range result.Arguments {
		results = append(results, item.(map[string]interface{})["name"].(string))
	}
	return results, nil
}
