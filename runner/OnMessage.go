package runner

import (
	"log"
	"regexp"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

// OnMessageSubscription Handler for detection and calculation of math in messages
// event.chat.message
var OnMessageSubscription = lib.Subscription{
	Topic:   "event.chat.message",
	Handler: onMessage,
}

// onMessage handles detection and calculation of math in messages
func onMessage(event *wamp.Event) {
	// If message exists
	if len(event.Arguments) > 0 {
		// Convert json message to twitch.PrivateMessage
		message := service.MapToPrivateMessage(event.Arguments[0].(map[string]interface{}))

		// Check if message is math
		regex := regexp.MustCompile(`^\s*\(?(\-?\+?[0-9.]+)\s*([+*-/%x^])\s*(\-?\+?[0-9.]+)\)?`)
		if match := regex.FindStringSubmatch(message.Message); match != nil {
			log.Print("Math detected")
			// Run math
			go RunMath(message, match[1], match[3], match[2])
		}
	}
}
