module gitlab.com/livesocket/math-runner

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/gempir/go-twitch-irc/v2 v2.2.1
	gitlab.com/livesocket/service v1.4.3
)
